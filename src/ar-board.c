/*
 * Copyright © 1998, 2003 Jonathan Blandford <jrb@mit.edu>
 * Copyright © 2007, 2008, 2009, 2010 Christian Persch
 *
 * Some code copied from gtk+/gtk/gtkiconview (LGPL2+):
 * Copyright © 2002, 2004  Anders Carlsson <andersca@gnu.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <string.h>
#include <math.h>

#include <gtk/gtk.h>

#include "ar-board.h"
#include "ar-marshal.h"
#include "ar-runtime.h"
#include "ar-sound.h"

#include "conf.h"
#include "game.h"
#include "ar-cursor.h"
#include "ar-card-surface-cache.h"
#include "ar-style-gtk.h"


/* The minimum size for the playing area. Almost completely arbitrary. */
#define BOARD_MIN_WIDTH 300
#define BOARD_MIN_HEIGHT 200

/* The limits for how much overlap there is between cards and
 * how much is allowed to slip off the bottom or right.
 */
#define MIN_DELTA (0.05)

#define DOUBLE_TO_INT_CEIL(d) ((int) (d + 0.5))

#define I_(string) g_intern_static_string (string)

typedef enum {
  STATUS_NONE,
  STATUS_MAYBE_DRAG,
  STATUS_NOT_DRAG,
  STATUS_IS_DRAG,
  STATUS_SHOW,
  LAST_STATUS
} MoveStatus;

typedef struct _ArBoardPrivate
{
  AisleriotGame *game;

  ArStyle *style;

  GdkCursor *cursor[AR_LAST_CURSOR];

  CardSize card_size;

  GtkAllocation allocation;

  double width;
  double height;

  /* The size of a slot in pixels. */
  double xslotstep;
  double yslotstep;

  /* How much of the slot the card should take up */
  double card_slot_ratio;

  /* The offset of the cards within the slot. */
  int xoffset, yoffset;

  /* The offset within the window. */
  int xbaseoffset;

  /* Cards cache */
  ArCardSurfaceCache *card_cache;
  cairo_surface_t *slot_surface;

  /* Button press */
  int last_click_x;
  int last_click_y;
  guint32 last_click_time;

  /* Moving cards */
  ArSlot *moving_cards_origin_slot;
  int moving_cards_origin_card_id; /* The index of the card that was clicked on in hslot->cards; or -1 if the click wasn't on a card */
  ArSlot *moving_cards_slot; /* if non-NULL, a move is in progress */

  /* The 'reveal card' action's slot and card link */
  ArSlot *show_card_slot;
  int show_card_id;

  /* Click data */
  ArSlot *last_clicked_slot;
  int last_clicked_card_id;

  /* Focus handling */
  ArSlot *focus_slot;
  int focus_card_id; /* -1 for focused empty slot */
  GdkRectangle focus_rect;

  /* Selection */
  ArSlot *selection_slot;
  int selection_start_card_id;
  GdkRectangle selection_rect;

  /* Highlight */
  ArSlot *highlight_slot;

  /* Status message */
  const char *status_message; /* interned */

  /* Bit field */
  guint droppable_supported : 1;
  guint show_focus : 1; /* whether the focus is drawn */

  guint click_to_move : 1;

  guint geometry_set : 1;
  guint is_rtl : 1;

  guint last_click_left_click : 1;
  guint click_status : 4; /* enough bits for MoveStatus */

  guint show_selection : 1;
  guint show_highlight : 1;
  guint show_status_messages : 1;

  guint force_geometry_update : 1;
} ArBoardPrivate;

G_STATIC_ASSERT (LAST_STATUS < 16 /* 2^4 */);

enum
{
  PROP_0,
  PROP_GAME,
  PROP_STYLE
};

enum
{
  STATUS_MESSAGE,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void get_slot_and_card_from_point (ArBoard *board,
                                          int x,
                                          int y,
                                          ArSlot **slot,
                                          int *_cardid);
static void slot_update_card_images      (ArBoard *board,
                                          ArSlot *slot);
static void slot_update_card_images_full (ArBoard *board,
                                          ArSlot *slot,
                                          int highlight_start_card_id);


G_DEFINE_TYPE_WITH_PRIVATE (ArBoard, ar_board, GTK_TYPE_DRAWING_AREA);


/* Cursor */

static void
set_cursor (ArBoard *board,
            ArCursorType cursor)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (board)),
                         priv->cursor[cursor]);
}

/* If we are over a slot, set the cursor to the given cursor,
 * otherwise use the default cursor. */
static void
set_cursor_by_location (ArBoard *board,
                        int x,
                        int y)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *selection_slot = priv->selection_slot;
  int selection_start_card_id = priv->selection_start_card_id;
  ArSlot *slot;
  int card_id;
  gboolean drop_valid = FALSE;
  ArCursorType cursor = AR_CURSOR_DEFAULT;

  get_slot_and_card_from_point (board, x, y, &slot, &card_id);

  if (priv->click_status == STATUS_IS_DRAG &&
      slot != NULL &&
      selection_slot != NULL &&
      slot != selection_slot &&
      selection_start_card_id >= 0) {
    g_return_if_fail (selection_slot->cards->len > (guint) selection_start_card_id);

    drop_valid = aisleriot_game_drop_valid (priv->game,
                                            selection_slot->id,
                                            slot->id,
                                            selection_slot->cards->data + selection_start_card_id,
                                            selection_slot->cards->len - selection_start_card_id);
  }
  /* FIXMEchpe: special cursor when _drag_ is possible? */

  if (drop_valid) {
    cursor = AR_CURSOR_DROPPABLE;
  } else if (slot != NULL &&
             card_id >= 0 &&
             !CARD_GET_FACE_DOWN (CARD (slot->cards->data[card_id]))) {
    if (priv->click_status == STATUS_NONE) {
      cursor = AR_CURSOR_OPEN;
    } else {
      cursor = AR_CURSOR_CLOSED;
    }
  }

  set_cursor (board, cursor);
}

/* status message */

static void
set_status_message (ArBoard *board,
                    const char *message)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  if (g_strcmp0 (priv->status_message, message) == 0)
    return;

  priv->status_message = g_intern_string (message);

  g_signal_emit (board, signals[STATUS_MESSAGE], 0, priv->status_message);
}

/* Slot helpers */

static void
get_slot_and_card_from_point (ArBoard *board,
                              int x,
                              int y,
                              ArSlot **slot,
                              int *_cardid)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GPtrArray *slots;
  gboolean got_slot = FALSE;
  int num_cards;
  int i, n_slots;
  int cardid;

  *slot = NULL;
  cardid = -1;

  slots = aisleriot_game_get_slots (priv->game);

  n_slots = slots->len;
  for (i = n_slots - 1; i >= 0; --i) {
    ArSlot *hslot = slots->pdata[i];

    /* if point is within our rectangle */
    if (hslot->rect.x <= x && x <= hslot->rect.x + hslot->rect.width &&
        hslot->rect.y <= y && y <= hslot->rect.y + hslot->rect.height) {
      num_cards = hslot->cards->len;

      if (got_slot == FALSE || num_cards > 0) {
        /* if we support exposing more than one card,
         * find the exact card  */

        guint depth = 1;

        if (hslot->pixeldx > 0)
          depth += (x - hslot->rect.x) / hslot->pixeldx;
        else if (hslot->pixeldx < 0)
          depth += (hslot->rect.x + hslot->rect.width - x) / -hslot->pixeldx;
        else if (hslot->pixeldy > 0)
          depth += (y - hslot->rect.y) / hslot->pixeldy;

        /* account for the last card getting much more display area
         * or no cards */

        if (depth > hslot->exposed)
          depth = hslot->exposed;
        *slot = hslot;

        /* card = #cards in slot + card chosen (indexed in # exposed cards) - # exposed cards */

        cardid = num_cards + depth - hslot->exposed;

        /* this is the topmost slot with a card */
        /* take it and run */
        if (num_cards > 0)
          break;

        got_slot = TRUE;
      }
    }
  }

  *_cardid = cardid > 0 ? cardid - 1 : -1;
}

static void
get_rect_by_slot_and_card (ArBoard *board,
                           ArSlot *slot,
                           int card_id,
                           int num_cards,
                           GdkRectangle *rect)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  guint delta;
  int first_card_id, num;

  g_return_if_fail (slot != NULL && card_id >= -1);

  first_card_id = ((int) slot->cards->len) - ((int) slot->exposed);

  if (card_id >= first_card_id) {
    delta = card_id - first_card_id;
    num = num_cards - 1;

    rect->x = slot->rect.x + delta * slot->pixeldx;
    rect->y = slot->rect.y + delta * slot->pixeldy;
    rect->width = priv->card_size.width + num * slot->pixeldx;
    rect->height = priv->card_size.height + num * slot->pixeldy;
        
    if (priv->is_rtl &&
        slot->expanded_right) {
      rect->x += slot->rect.width - priv->card_size.width;
    }

  } else {
    /* card_id == -1 or no card available, return the slot rect.
     * Its size should be card_size.
     */
    *rect = slot->rect;
  }
}

/* Focus handling */

static void
widen_rect (GdkRectangle *rect,
            int delta)
{
  int x, y;

  x = rect->x - delta;
  y = rect->y - delta;

  rect->x = MAX (x, 0);
  rect->y = MAX (y, 0);
  rect->width = rect->width + 2 * delta;
  rect->height = rect->height + 2 * delta;
}

static void
get_focus_rect (ArBoard *board,
                GdkRectangle *rect)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  if (!priv->focus_slot)
    return;

  get_rect_by_slot_and_card (board,
                             priv->focus_slot,
                             priv->focus_card_id,
                             1, rect);
  widen_rect (rect, ar_style_get_focus_line_width (priv->style)+
                    ar_style_get_focus_padding (priv->style));
}

static void
set_focus (ArBoard *board,
           ArSlot *slot,
           int card_id,
           gboolean show_focus)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  GdkWindow *window;
  int top_card_id;

  /* Sanitise */
  top_card_id = slot ? ((int) slot->cards->len) - 1 : -1;
  card_id = MIN (card_id, top_card_id);

  if (priv->focus_slot == slot &&
      priv->focus_card_id == card_id &&
      priv->show_focus == show_focus)
    return;

  window = gtk_widget_get_window (widget);

  if (priv->focus_slot != NULL) {
    if (priv->show_focus &&
        gtk_widget_has_focus (widget)) {
      gdk_window_invalidate_rect (window, &priv->focus_rect, FALSE);
    
      priv->show_focus = FALSE;
    }

    priv->focus_slot = NULL;
    priv->focus_card_id = -1;
  }

  priv->show_focus = show_focus;

  if (!slot)
    return;

  priv->focus_slot = slot;
  priv->focus_card_id = card_id;

  if (show_focus &&
      gtk_widget_has_focus (widget)) {
    get_focus_rect (board, &priv->focus_rect);
    gdk_window_invalidate_rect (window, &priv->focus_rect, FALSE);
  }
}

/* Selection handling */

static void
get_selection_rect (ArBoard *board,
                    GdkRectangle *rect)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  int n_cards;

  if (!priv->selection_slot)
    return;

  n_cards = priv->selection_slot->cards->len - priv->selection_start_card_id;

  get_rect_by_slot_and_card (board,
                             priv->selection_slot,
                             priv->selection_start_card_id,
                             n_cards, rect);
}

static void
set_selection (ArBoard *board,
               ArSlot *slot,
               int card_id,
               gboolean show_selection)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  GdkWindow *window;

  if (priv->selection_slot == slot &&
      priv->selection_start_card_id == card_id &&
      priv->show_selection == show_selection)
    return;

  window = gtk_widget_get_window (widget);

  if (priv->selection_slot != NULL) {
    if (priv->show_selection) {
      gdk_window_invalidate_rect (window, &priv->selection_rect, FALSE);

      /* Clear selection card images */
      slot_update_card_images_full (board, priv->selection_slot, G_MAXINT);
    }

    priv->selection_slot = NULL;
    priv->selection_start_card_id = -1;
  }

  priv->show_selection = show_selection;
  priv->selection_slot = slot;
  priv->selection_start_card_id = card_id;
  g_assert (slot != NULL || card_id == -1);

  if (!slot)
    return;

  g_assert (card_id < 0 || card_id < (gint) slot->cards->len);

  if (priv->show_selection) {
    get_selection_rect (board, &priv->selection_rect);
    gdk_window_invalidate_rect (window, &priv->selection_rect, FALSE);
  
    slot_update_card_images_full (board, slot, card_id);
  }
}

/* Slot functions */

static void
slot_update_geometry (ArBoard *board,
                      ArSlot *slot)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  GdkRectangle old_rect;
  GByteArray *cards;
  int delta, xofs, yofs, pixeldx;
  double card_step;

  if (!priv->geometry_set)
    return;

  cards = slot->cards;
  old_rect = slot->rect;

  card_step = ar_style_get_card_step (priv->style);

  xofs = priv->xoffset;
  yofs = priv->yoffset;

  if (priv->is_rtl) {
    slot->rect.x = priv->xslotstep * (priv->width - slot->x) - priv->card_size.width - xofs + priv->xbaseoffset;
  } else {
    slot->rect.x = priv->xslotstep * slot->x + xofs + priv->xbaseoffset;
  }

  slot->rect.y = priv->yslotstep * slot->y + yofs; /* FIXMEchpe + priv->ybaseoffset; */

  /* We need to make sure the cards fit within the board, even
   * when there are many of them. See bug #171417.
   */
  /* FIXMEchpe: check |slot->exposed| instead of cards->len? */
  pixeldx = 0;
  if (cards->len > 1) {
    double dx = 0, dy = 0;
    double n_cards = cards->len - 1; /* FIXMEchpe: slot->exposed - 1 ? */

    if (slot->expanded_down) {
      double y_from_bottom, max_dy = card_step;

      if (slot->dy_set)
        max_dy = slot->expansion.dy;

      /* Calculate the compressed_dy that will let us fit within the board */
      y_from_bottom = ((double) (priv->allocation.height - slot->rect.y)) / ((double) priv->card_size.height);
      dy = (y_from_bottom - (1.0 - ar_style_get_card_overhang (priv->style))) / n_cards;
      dy = CLAMP (dy, MIN_DELTA, max_dy);
    } else if (slot->expanded_right) {
      if (priv->is_rtl) {
        double x_from_left, max_dx = card_step;

        if (slot->dx_set)
          max_dx = slot->expansion.dx;

        x_from_left = ((double) slot->rect.x) / ((double) priv->card_size.width) + 1.0;
        dx = (x_from_left - (1.0 - ar_style_get_card_overhang (priv->style))) / n_cards;
        dx = CLAMP (dx, MIN_DELTA, max_dx);

        slot->pixeldx = DOUBLE_TO_INT_CEIL (- dx * priv->card_size.width);
        pixeldx = -slot->pixeldx;
      } else {
        double x_from_right, max_dx = card_step;

        if (slot->dx_set)
          max_dx = slot->expansion.dx;

        x_from_right = ((double) (priv->allocation.width - slot->rect.x)) / ((double) priv->card_size.width);
        dx = (x_from_right - (1.0 - ar_style_get_card_overhang (priv->style))) / n_cards;
        dx = CLAMP (dx, MIN_DELTA, max_dx);

        pixeldx = slot->pixeldx = DOUBLE_TO_INT_CEIL (dx * priv->card_size.width);        
      }
    }

    slot->pixeldy = DOUBLE_TO_INT_CEIL (dy * priv->card_size.height);
  } else {
    slot->pixeldx = slot->pixeldy = 0;
  }

  slot->exposed = cards->len;
  if (0 < slot->expansion_depth &&
      slot->expansion_depth < slot->exposed) {
    slot->exposed = slot->expansion_depth;
  }

  if ((slot->pixeldx == 0 &&
       slot->pixeldy == 0 &&
       slot->exposed > 1) ||
      (cards->len > 0 &&
       slot->exposed < 1)) {
    slot->exposed = 1;
  }

  delta = slot->exposed > 0 ? slot->exposed - 1 : 0;

  slot->rect.width = priv->card_size.width + delta * pixeldx;
  slot->rect.height = priv->card_size.height + delta * slot->pixeldy;

  if (priv->is_rtl) {
    slot->rect.x -= slot->rect.width - priv->card_size.width;
  }

  if (gtk_widget_get_realized (widget)) {
    GdkRectangle damage = slot->rect;

    if (old_rect.width > 0 && old_rect.height > 0) {
      gdk_rectangle_union (&damage, &old_rect, &damage);
    }

    gdk_window_invalidate_rect (gtk_widget_get_window (widget), &damage, FALSE);
  }

  slot->needs_update = FALSE;
}

static void
slot_update_card_images_full (ArBoard *board,
                              ArSlot *slot,
                              int highlight_start_card_id)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GPtrArray *card_images;
  guint n_cards, first_exposed_card_id, i;
  guint8 *cards;
  ArCardSurfaceCache *card_cache = priv->card_cache;

  card_images = slot->card_images;
  g_ptr_array_set_size (card_images, 0);

  if (!priv->geometry_set)
    return;

  cards = slot->cards->data;
  n_cards = slot->cards->len;

  g_assert (n_cards >= slot->exposed);
  first_exposed_card_id = n_cards - slot->exposed;

  /* No need to get invisible cards from cache, which will just
   * slow us down!
   */
  for (i = 0; i < first_exposed_card_id; ++i) {
    g_ptr_array_add (card_images, NULL);
  }

  for (i = first_exposed_card_id; i < n_cards; ++i) {
    Card card = CARD (cards[i]);

    g_ptr_array_add (card_images,
                     ar_card_surface_cache_get_card_surface (card_cache, card));
  }
}

static void
slot_update_card_images (ArBoard *board,
                         ArSlot *slot)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  int highlight_start_card_id = G_MAXINT;

  if (G_UNLIKELY (slot == priv->highlight_slot &&
                  priv->show_highlight)) {
    highlight_start_card_id = slot->cards->len - 1;
  } else if (G_UNLIKELY (slot == priv->selection_slot &&
                         priv->selection_start_card_id >= 0 &&
                         priv->show_selection)) {
    highlight_start_card_id = priv->selection_start_card_id;
  }

  slot_update_card_images_full (board, slot, highlight_start_card_id);
}

/* helper functions */

static void
ar_board_error_bell (ArBoard *board)
{
  gtk_widget_error_bell (GTK_WIDGET (board));
}

/* Work out new sizes and spacings for the cards. */
static void
ar_board_setup_geometry (ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  GPtrArray *slots;
  guint i, n_slots;
  CardSize card_size;
  ArCardTheme *theme;

  /* Nothing to do yet */
  if (aisleriot_game_get_state (priv->game) <= GAME_LOADED)
    return;

  g_return_if_fail (gtk_widget_get_realized (widget));
  g_return_if_fail (priv->width > 0 && priv->height > 0);

  priv->xslotstep = ((double) priv->allocation.width) / priv->width;
  priv->yslotstep = ((double) priv->allocation.height) / priv->height;

  theme = ar_style_get_card_theme (priv->style);
  if (theme == NULL)
    return;

  ar_card_theme_set_size (theme,
                                         priv->xslotstep,
                                         priv->yslotstep,
                                         priv->card_slot_ratio);
  ar_card_theme_get_size (theme, &card_size);

  priv->card_size = card_size;

  /* If the cards are too far apart, bunch them in the middle. */
  priv->xbaseoffset = 0;
  if (priv->xslotstep > (card_size.width * 3) / 2) {
    priv->xslotstep = (card_size.width * 3) / 2;
    /* FIXMEchpe: if there are expand-right slots, reserve the space for them instead? */
    priv->xbaseoffset = (priv->allocation.width - priv->xslotstep * priv->width) / 2;
  }
  if (priv->yslotstep > (card_size.height * 3) / 2) {
    priv->yslotstep = (card_size.height * 3) / 2;
    /* FIXMEchpe: if there are expand-down slots, reserve the space for them instead?
       priv->ybaseoffset = (priv->allocation.height - priv->yslotstep * priv->height) / 2;
    */
  }

  priv->xoffset = (priv->xslotstep - card_size.width) / 2;
  priv->yoffset = (priv->yslotstep - card_size.height) / 2;

  priv->slot_surface = ar_card_surface_cache_get_slot_surface (priv->card_cache);

  /* NOTE! Updating the slots checks that geometry is set, so
   * we set it to TRUE already.
   */
  priv->geometry_set = TRUE;

  /* Now recalculate the slot locations. */
  slots = aisleriot_game_get_slots (priv->game);

  n_slots = slots->len;
  for (i = 0; i < n_slots; ++i) {
    ArSlot *slot = slots->pdata[i];

    slot_update_geometry (board, slot);
    slot_update_card_images (board, slot);
  }

  if (priv->moving_cards_slot != NULL) {
    slot_update_geometry (board, priv->moving_cards_slot);
    slot_update_card_images (board, priv->moving_cards_slot);
  }

  /* Update the focus and selection rects */
  get_focus_rect (board, &priv->focus_rect);
  get_selection_rect (board, &priv->selection_rect);
}

static void
drag_begin (ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *hslot, *mslot;
  int delta;
  int x, y;
  int num_moving_cards;
  GByteArray *cards;

  if (!priv->selection_slot ||
      priv->selection_start_card_id < 0) {
    priv->click_status = STATUS_NONE;
    return;
  }

  priv->click_status = STATUS_IS_DRAG;

  hslot = priv->moving_cards_origin_slot = priv->selection_slot;
  priv->moving_cards_origin_card_id = priv->selection_start_card_id;

  num_moving_cards = hslot->cards->len - priv->moving_cards_origin_card_id;

  cards = hslot->cards;

  /* Save game state */
  aisleriot_game_record_move (priv->game, hslot->id,
                              cards->data, cards->len);

  /* Unset the selection and focus. It'll be re-set if the drag is aborted */
  set_selection (board, NULL, -1, FALSE);
  set_focus (board, NULL, -1, FALSE);

  delta = hslot->exposed - num_moving_cards;

  /* (x,y) is the upper left edge of the topmost dragged card */
  x = hslot->rect.x + delta * hslot->pixeldx;
  if (priv->is_rtl &&
      hslot->expanded_right) {
    x += hslot->rect.width - priv->card_size.width;
  }

  priv->last_click_x -= x;
  priv->last_click_y -= y = hslot->rect.y + delta * hslot->pixeldy;;

  /* Create temporary slot for moving cards */
  priv->moving_cards_slot = mslot = g_slice_dup (ArSlot, hslot);
  mslot->cards = g_byte_array_sized_new (SLOT_CARDS_N_PREALLOC);
  g_byte_array_append (mslot->cards,
                       cards->data + priv->moving_cards_origin_card_id,
                       cards->len - priv->moving_cards_origin_card_id);
  mslot->expansion_depth = 0;
  mslot->exposed = mslot->cards->len;
  mslot->id = -1;
  mslot->x = mslot->y = 0.;
  mslot->card_images = g_ptr_array_sized_new (SLOT_CARDS_N_PREALLOC);
  mslot->needs_update = TRUE;
  slot_update_geometry (board, mslot);
  slot_update_card_images_full (board, mslot, G_MAXINT);
  mslot->rect.x = x;
  mslot->rect.y = y;
  
  /* Take the cards off of the stack */
  g_byte_array_set_size (cards, priv->moving_cards_origin_card_id);
  g_ptr_array_set_size (hslot->card_images, priv->moving_cards_origin_card_id);
  slot_update_geometry (board, hslot);
  slot_update_card_images (board, hslot);

  /* Change cursor */
  set_cursor (board, AR_CURSOR_CLOSED);
}

static void
drag_end (ArBoard *board,
          gboolean moved)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);

  /* FIXMEchpe: check that slot->cards->len == moving_cards_origin_card_id !!! FIXMEchpe what to do if not, abort the game? */
  /* Add the origin cards back to the origin slot */
  if (!moved &&
      priv->moving_cards_origin_slot != NULL &&
      priv->moving_cards_slot != NULL &&
      priv->moving_cards_slot->cards->len > 0) {
    aisleriot_game_slot_add_cards (priv->game,
                                   priv->moving_cards_origin_slot,
                                   priv->moving_cards_slot->cards->data,
                                   priv->moving_cards_slot->cards->len);
  }

  priv->click_status = STATUS_NONE;
  priv->moving_cards_origin_slot = NULL;
  priv->moving_cards_origin_card_id = -1;

  if (priv->moving_cards_slot != NULL) {
    GdkRectangle *rect = &priv->moving_cards_slot->rect;

    gtk_widget_queue_draw_area (widget, rect->x, rect->y, rect->width, rect->height);

    g_byte_array_free (priv->moving_cards_slot->cards, TRUE);
    g_ptr_array_free (priv->moving_cards_slot->card_images, TRUE);
    g_slice_free (ArSlot, priv->moving_cards_slot);
    priv->moving_cards_slot = NULL;
  }
}

static gboolean
cards_are_droppable (ArBoard *board,
                     ArSlot *slot)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  return slot != NULL &&
         priv->moving_cards_origin_slot != NULL &&
         priv->moving_cards_slot != NULL &&
         aisleriot_game_drop_valid (priv->game,
                                    priv->moving_cards_origin_slot->id,
                                    slot->id,
                                    priv->moving_cards_slot->cards->data,
                                    priv->moving_cards_slot->cards->len);
}

static ArSlot *
find_drop_target (ArBoard *board,
                  gint x,
                  gint y)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *new_hslot;
  ArSlot *retval = NULL;
  gint i, new_cardid;
  gint min_distance = G_MAXINT;

  /* Find a target directly under the center of the card. */
  get_slot_and_card_from_point (board,
                                x + priv->card_size.width / 2,
                                y + priv->card_size.height / 2,
                                &new_hslot, &new_cardid);

  if (cards_are_droppable (board, new_hslot))
    return new_hslot;

  /* If that didn't work, look for a target at all 4 corners of the card. */
  for (i = 0; i < 4; i++) {
    get_slot_and_card_from_point (board,
                                  x + priv->card_size.width * (i / 2),
                                  y + priv->card_size.height * (i % 2),
                                  &new_hslot, &new_cardid);

    if (!new_hslot)
      continue;

    /* This skips corners we know are not droppable. */
    if (!priv->droppable_supported || cards_are_droppable (board, new_hslot)) {
      gint dx, dy, distance_squared;

      dx = new_hslot->rect.x + (new_cardid - 1) * new_hslot->pixeldx - x;
      dy = new_hslot->rect.y + (new_cardid - 1) * new_hslot->pixeldy - y;

      distance_squared = dx * dx + dy * dy;

      if (distance_squared <= min_distance) {
	retval = new_hslot;
	min_distance = distance_squared;
      }
    }
  }

  return retval;
}

static void
drop_moving_cards (ArBoard *board,
                   gint x,
                   gint y)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *hslot;
  gboolean moved = FALSE;

  hslot = find_drop_target (board,
                            x - priv->last_click_x,
                            y - priv->last_click_y);

  if (hslot) {
    moved = aisleriot_game_drop_cards (priv->game,
                                       priv->moving_cards_origin_slot->id,
                                       hslot->id,
                                       priv->moving_cards_slot->cards->data,
                                       priv->moving_cards_slot->cards->len);
  }

  if (moved) {
    aisleriot_game_end_move (priv->game);
    ar_sound_play ("click");
  } else {
    aisleriot_game_discard_move (priv->game);
    ar_sound_play ("slide");
  }

  drag_end (board, moved);

  if (moved)
    aisleriot_game_test_end_of_game (priv->game);
}

static void
highlight_drop_target (ArBoard *board,
                       ArSlot *slot)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  GdkWindow *window;
  GdkRectangle rect;

  if (slot == priv->highlight_slot)
    return;

  window = gtk_widget_get_window (widget);

  /* Invalidate the old highlight rect */
  if (priv->highlight_slot != NULL &&
      priv->show_highlight) {
    get_rect_by_slot_and_card (board,
                               priv->highlight_slot,
                               priv->highlight_slot->cards->len - 1 /* it's ok if this is == -1 */,
                               1, &rect);
    gdk_window_invalidate_rect (window, &rect, FALSE);

    /* FIXMEchpe only update the topmost card? */
    /* It's ok to call this directly here, since the old highlight_slot cannot
     * have been the same as the current selection_slot!
     */
    slot_update_card_images_full (board, priv->highlight_slot, G_MAXINT);
  }

  /* Need to set the highlight slot even when we the cards aren't droppable
   * since that can happen when the game doesn't support FEATURE_DROPPABLE.
   */
  priv->highlight_slot = slot;
  
  if (!cards_are_droppable (board, slot))
    return;

  if (!priv->show_highlight)
    return;

  /* Prepare the highlight pixbuf/pixmaps and invalidate the new highlight rect */
  get_rect_by_slot_and_card (board,
                             slot,
                             slot->cards->len - 1 /* it's ok if this is == -1 */,
                             1, &rect);
  gdk_window_invalidate_rect (window, &rect, FALSE);

  /* FIXMEchpe only update the topmost card? */
  /* It's ok to call this directly, since the highlight slot is always
   * different from the selection slot!
   */
  slot_update_card_images_full (board, slot, ((int) slot->cards->len) - 1);
}

static void
reveal_card (ArBoard *board,
             ArSlot *slot,
             guint cardid)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  Card card;
  GdkRectangle rect;
  GdkWindow *window;

  if (priv->show_card_slot == slot)
    return;

  window = gtk_widget_get_window (widget);

  if (priv->show_card_slot != NULL) {
    get_rect_by_slot_and_card (board,
                               priv->show_card_slot,
                               priv->show_card_id,
                               1, &rect);
    gdk_window_invalidate_rect (window, &rect, FALSE);
    priv->show_card_slot = NULL;
    priv->show_card_id = -1;
    priv->click_status = STATUS_NONE;
  }

  if (!slot || cardid >= slot->cards->len - 1)
    return;

  card = CARD (slot->cards->data[cardid]);
  if (CARD_GET_FACE_DOWN (card))
    return;

  priv->show_card_slot = slot;
  priv->show_card_id = cardid;
  priv->click_status = STATUS_SHOW;

  get_rect_by_slot_and_card (board,
                            priv->show_card_slot,
                            priv->show_card_id,
                            1, &rect);
  gdk_window_invalidate_rect (window, &rect, FALSE);
}

static void
clear_state (ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  highlight_drop_target (board, NULL);
  drag_end (board, FALSE /* FIXMEchpe ? */);

  reveal_card (board, NULL, -1);

  priv->click_status = STATUS_NONE;
  priv->last_clicked_slot = NULL;
  priv->last_clicked_card_id = -1;
}

/* Note: this unsets the selection! hslot may be equal to priv->selection_slot. */
static gboolean
ar_board_move_selected_cards_to_slot (ArBoard *board,
                                             ArSlot *hslot)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *selection_slot = priv->selection_slot;
  guint selection_start_card_id = priv->selection_start_card_id;
  gboolean moved;
  guint8 *cards;
  guint n_cards;

  if (!selection_slot ||
      priv->selection_start_card_id < 0)
    return FALSE;

  /* NOTE: We cannot use aisleriot_game_drop_valid here since the
   * game may not support the "droppable" feature.
   */

  set_selection (board, NULL, -1, FALSE);

  priv->click_status = STATUS_NONE;

  aisleriot_game_record_move (priv->game,
                              selection_slot->id,
                              selection_slot->cards->data,
                              selection_slot->cards->len);

  /* Store the cards, since the move could alter slot->cards! */
  g_assert (selection_slot->cards->len >= selection_start_card_id);
  n_cards = selection_slot->cards->len - selection_start_card_id;

  cards = g_alloca (n_cards);
  memcpy (cards,
          selection_slot->cards->data + selection_start_card_id,
          n_cards);

  /* Now take the cards off of the origin slot. We'll update the slot geometry later */
  g_byte_array_set_size (selection_slot->cards, selection_start_card_id);
  selection_slot->needs_update = TRUE;

  moved = aisleriot_game_drop_cards (priv->game,
                                      selection_slot->id,
                                      hslot->id,
                                      cards,
                                      n_cards);
  if (moved) {
    aisleriot_game_end_move (priv->game);

    ar_sound_play ("click");

    if (selection_slot->needs_update)
      g_signal_emit_by_name (priv->game, "slot-changed", selection_slot); /* FIXMEchpe! */

    aisleriot_game_test_end_of_game (priv->game);
  } else {
    /* Not moved; discard the move add the cards back to the origin slot */
    aisleriot_game_discard_move (priv->game);
    aisleriot_game_slot_add_cards (priv->game, selection_slot, cards, n_cards);
  }

  return moved;
}

/* Tooltips */

static gboolean
ar_board_query_tooltip_cb (GtkWidget *widget,
                                  int x,
                                  int y,
                                  gboolean keyboard_mode,
                                  GtkTooltip *tooltip,
                                  ArBoard *board)
{
  ArSlot *slot;
  int cardid;
  char *text;
  GdkRectangle rect;

  get_slot_and_card_from_point (board, x, y, &slot, &cardid);
  if (!slot || ar_slot_get_slot_type (slot) == AR_SLOT_UNKNOWN)
    return FALSE;

  text = ar_slot_get_hint_string (slot, cardid);
  gtk_tooltip_set_text (tooltip, text);
  g_free (text);

  get_rect_by_slot_and_card (board, slot, cardid, 1, &rect);
  /* FIXMEchpe: subtract the rect of the next card, if there is one! */
  gtk_tooltip_set_tip_area (tooltip, &rect);

  return TRUE;
}

/* Keynav */


/* Game state handling */

static void
game_type_changed_cb (AisleriotGame *game,
                      ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  guint features;

  features = aisleriot_game_get_features (game);

  priv->droppable_supported = ((features & FEATURE_DROPPABLE) != 0);
  priv->show_highlight = priv->droppable_supported;
}

static void
game_cleared_cb (AisleriotGame *game,
                 ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  priv->geometry_set = FALSE;

  /* So we don't re-add the cards to the now-dead slot */
  priv->highlight_slot = NULL;
  priv->last_clicked_slot = NULL;
  priv->moving_cards_origin_slot = NULL;
  priv->selection_slot = NULL;
  priv->show_card_slot = NULL;

  clear_state (board);
}

static void
game_new_cb (AisleriotGame *game,
             ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  clear_state (board);

  set_focus (board, NULL, -1, FALSE);
  set_selection (board, NULL, -1, FALSE);

  aisleriot_game_get_geometry (game, &priv->width, &priv->height);

  ar_board_setup_geometry (board);

#if 0
  g_print ("{ %.3f , %.3f /* %s */ },\n",
           priv->width, priv->height,
           aisleriot_game_get_game_module (priv->game));
#endif

  gtk_widget_queue_draw (GTK_WIDGET (board));
}

static void
slot_changed_cb (AisleriotGame *game,
                 ArSlot *slot,
                 ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  slot_update_geometry (board, slot);
  slot_update_card_images (board, slot);

  if (slot == priv->moving_cards_origin_slot) {
    /* PANIC! */
    /* FIXMEchpe */
  }
  if (slot == priv->selection_slot) {
    set_selection (board, NULL, -1, FALSE);

    /* If this slot changes while we're in a click cycle, abort the action.
     * That prevents a problem where the cards that were selected and
     * about to be dragged have vanished because of autoplay; c.f. bug #449767.
     * Note: we don't use clear_state() here since we only want to disable
     * the highlight and revealed card if that particular slot changed, see
     * the code above. And we don't clear last_clicked_slot/card_id either, so
     * a double-click will still work.
     */
    priv->click_status = STATUS_NONE;
  }
  if (slot == priv->focus_slot) {
    /* Try to keep the focus intact. If the focused card isn't there
     * anymore, this will set the focus to the topmost card of there
     * same slot, or the slot itself if there are no cards on it.
     * If the slot was empty but now isn't, we set the focus to the
     * topmost card.
     */
    if (priv->focus_card_id < 0) {
      set_focus (board, slot, ((int) slot->cards->len) - 1, priv->show_focus);
    } else {
      set_focus (board, slot, priv->focus_card_id, priv->show_focus);
    }
  }
  if (slot == priv->highlight_slot) {
    highlight_drop_target (board, NULL);
  }
}

/* Class implementation */

/* ArBoardClass methods */


/* GtkWidgetClass methods */
static void
ar_board_realize (GtkWidget *widget)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GdkWindow *window;
  GdkDisplay *display;

  GTK_WIDGET_CLASS (ar_board_parent_class)->realize (widget);

  window = gtk_widget_get_window (widget);

  ar_card_surface_cache_set_drawable (priv->card_cache, window);

  display = gtk_widget_get_display (widget);

  /* Create cursors */
  priv->cursor[AR_CURSOR_DEFAULT] = ar_cursor_new (display, AR_CURSOR_DEFAULT);
  priv->cursor[AR_CURSOR_OPEN] = ar_cursor_new (display, AR_CURSOR_OPEN);
  priv->cursor[AR_CURSOR_CLOSED] = ar_cursor_new (display, AR_CURSOR_CLOSED);
  priv->cursor[AR_CURSOR_DROPPABLE] = ar_cursor_new (display, AR_CURSOR_DROPPABLE);

  ar_board_setup_geometry (board);
}

static void
ar_board_unrealize (GtkWidget *widget)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  guint i;

  priv->geometry_set = FALSE;

  ar_card_surface_cache_set_drawable (priv->card_cache, NULL);
  priv->slot_surface = NULL;

  for (i = 0; i < AR_LAST_CURSOR; ++i) {
    g_object_unref (priv->cursor[i]);
    priv->cursor[i] = NULL;
  }

  clear_state (board);

  GTK_WIDGET_CLASS (ar_board_parent_class)->unrealize (widget);
}

/* Style handling */

static void
ar_board_sync_style (ArStyle *style,
                            GParamSpec *pspec,
                            ArBoard *board)
{
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkWidget *widget = GTK_WIDGET (board);
  const char *pspec_name;
  gboolean update_geometry = FALSE, redraw_focus = FALSE, queue_redraw = FALSE, redraw_selection = FALSE;
  gboolean realized;

  g_assert (style == priv->style);

  if (pspec != NULL) {
    pspec_name = pspec->name;
  } else {
    pspec_name = NULL;
  }

  realized = gtk_widget_get_realized (widget);

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_CARD_THEME)) {
    ArCardTheme *theme;

    theme = ar_style_get_card_theme (style);
    if (theme != NULL) {
      priv->geometry_set = FALSE;

      priv->slot_surface = NULL;
      ar_card_surface_cache_set_theme (priv->card_cache, theme);

      update_geometry |= TRUE;
      queue_redraw |= TRUE;
    }
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_CARD_SLOT_RATIO)) {
    double card_slot_ratio;

    card_slot_ratio = ar_style_get_card_slot_ratio (style);

    update_geometry |= (card_slot_ratio != priv->card_slot_ratio);

    priv->card_slot_ratio = card_slot_ratio;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_CARD_OVERHANG)) {
    update_geometry |= TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_CARD_STEP)) {
    update_geometry |= TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_INTERIOR_FOCUS)) {
    redraw_focus = TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_FOCUS_LINE_WIDTH)) {
    redraw_focus = TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_FOCUS_PADDING)) {
    redraw_focus = TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_RTL)) {
    gboolean is_rtl;

    is_rtl = ar_style_get_rtl (style);

    update_geometry |= (is_rtl != priv->is_rtl);

    priv->is_rtl = is_rtl;

    /* FIXMEchpe: necessary? */
    priv->force_geometry_update = TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_CLICK_TO_MOVE)) {
    gboolean click_to_move;

    click_to_move = ar_style_get_click_to_move (style);
    if (click_to_move != priv->click_to_move) {
      /* Clear the selection. Do this before setting the new value,
       * since otherwise selection won't get cleared correctly.
       */
      set_selection (board, NULL, -1, FALSE);

      priv->click_to_move = click_to_move;

      /* FIXMEchpe: we queue a redraw here. WHY?? Check that it's safe not to. */
      queue_redraw = TRUE;
    }
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_SELECTION_COLOR)) {
    redraw_selection = TRUE;
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_SHOW_TOOLTIPS)) {
    gtk_widget_set_has_tooltip (widget, ar_style_get_show_tooltips (priv->style));
  }

  if (pspec_name == NULL || pspec_name == I_(AR_STYLE_PROP_SHOW_STATUS_MESSAGES)) {
    gboolean show_status_messages;

    show_status_messages = ar_style_get_show_status_messages (priv->style);

    if (show_status_messages != priv->show_status_messages) {
      priv->show_status_messages = show_status_messages;

      if (!show_status_messages) {
        /* Clear message */
        set_status_message (board, NULL);
      }
    }
  }

  if (update_geometry && realized) {
    ar_board_setup_geometry (board);
  }

  if (queue_redraw && realized) {
    gtk_widget_queue_draw (widget);
  }

  if (redraw_focus) {
    /* FIXMEchpe: do redraw the focus! */
  }

  if (redraw_selection) {
    /* FIXMEchpe: do redraw the selection! */
  }
}

static void
ar_board_size_allocate (GtkWidget *widget,
                               GtkAllocation *allocation)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  gboolean is_same;

  is_same = (memcmp (&priv->allocation, allocation, sizeof (GtkAllocation)) == 0);
  
  GTK_WIDGET_CLASS (ar_board_parent_class)->size_allocate (widget, allocation);

  if (is_same && !priv->force_geometry_update)
    return;

  priv->force_geometry_update = FALSE;

  priv->allocation.x = allocation->x;
  priv->allocation.y = allocation->y;
  priv->allocation.width = allocation->width;
  priv->allocation.height = allocation->height;

  if (gtk_widget_get_realized (widget)) {
    ar_board_setup_geometry (board);
  }
}

static void
ar_board_get_preferred_width (GtkWidget *widget,
                                     gint      *minimum,
                                     gint      *natural)
{
  *minimum = *natural = BOARD_MIN_WIDTH;
}

static void
ar_board_get_preferred_height (GtkWidget *widget,
                                      gint      *minimum,
                                      gint      *natural)
{
  *minimum = *natural = BOARD_MIN_HEIGHT;
}


/* The gtkwidget.c focus in/out handlers queue a shallow draw;
 * that's ok for us but maybe we want to optimise this a bit to
 * only do it if we have a focus to draw/erase?
 */
static gboolean
ar_board_focus_in (GtkWidget *widget,
                          GdkEventFocus *event)
{
  return FALSE;
}

static gboolean
ar_board_focus_out (GtkWidget *widget,
                           GdkEventFocus *event)
{
  ArBoard *board = AR_BOARD (widget);

  clear_state (board);

  return FALSE;
}

static gboolean
ar_board_button_press (GtkWidget *widget,
                              GdkEventButton *event)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  ArSlot *hslot;
  int cardid;
  guint button;
  gboolean drag_valid;
  guint state;
  gboolean is_double_click, show_focus;

  /* NOTE: It's ok to just return instead of chaining up, since the
   * parent class has no class closure for this event.
   */

  /* ignore the gdk synthetic double/triple click events */
  if (event->type != GDK_BUTTON_PRESS)
    return FALSE;

  /* Don't do anything if a modifier is pressed */
  state = event->state & gtk_accelerator_get_default_mod_mask ();
  if (state != 0)
    return FALSE;

  button = event->button;

  /* We're only interested in left, middle and right-clicks */
  if (button < 1 || button > 3)
    return FALSE;

  /* If we already have a click, ignore this new one */
  if (priv->click_status != STATUS_NONE) {
    return TRUE;
  }

  /* If the game hasn't started yet, start it now */
  aisleriot_game_start (priv->game);

  get_slot_and_card_from_point (board, event->x, event->y, &hslot, &cardid);

  is_double_click = button == 2 ||
                    (priv->last_click_left_click &&
                     (event->time - priv->last_click_time <= (guint32)ar_style_get_double_click_time (priv->style)) &&
                     priv->last_clicked_slot == hslot &&
                     priv->last_clicked_card_id == cardid);

  priv->last_click_x = event->x;
  priv->last_click_y = event->y;
  priv->last_clicked_slot = hslot;
  priv->last_clicked_card_id = cardid;
  priv->last_click_time = event->time;
  priv->last_click_left_click = button == 1;

  if (!hslot) {
    set_focus (board, NULL, -1, FALSE);
    set_selection (board, NULL, -1, FALSE);

    priv->click_status = STATUS_NONE;

    return FALSE;
  }

  set_cursor (board, AR_CURSOR_CLOSED);

  /* First check if it's a right-click: if so, we reveal the card and do nothing else */
  if (button == 3) {
    /* Don't change the selection here! */
    reveal_card (board, hslot, cardid);

    return TRUE;
  }

  /* Clear revealed card */
  reveal_card (board, NULL, -1);

  /* We can't let Gdk do the double-click detection; since the entire playing
   * area is one big widget it can't distinguish between single-clicks on two
   * different cards and a double-click on one card.
   */
  if (is_double_click) {
    ArSlot *clicked_slot = hslot;

    priv->click_status = STATUS_NONE;

    /* Reset this since otherwise 3 clicks will be interpreted as 2 double-clicks */
    priv->last_click_left_click = FALSE;

    aisleriot_game_record_move (priv->game, -1, NULL, 0);
    if (aisleriot_game_button_double_clicked_lambda (priv->game, clicked_slot->id)) {
      aisleriot_game_end_move (priv->game);
    } else {
      aisleriot_game_discard_move (priv->game);
    }

    aisleriot_game_test_end_of_game (priv->game);

    set_cursor (board, AR_CURSOR_OPEN);

    return TRUE;
  }

  /* button == 1 from now on */

  if (priv->selection_slot == NULL)
    goto set_selection;

  /* In click-to-move mode, we need to test whether moving the selected cards
   * to this slot does a move. Note that it is necessary to do this both if
   * the clicked slot is the selection_slot (and the clicked card the topmost
   * card below the selection), and if it's not the selection_slot, since some
   * games depend on this behaviour (e.g. Treize). See bug #565560.
   *
   * Note that ar_board_move_selected_cards_to_slot unsets the selection,
   * so we need to fall through to set_selection if no move was done.
    */
  if (priv->click_to_move &&
      priv->selection_start_card_id >= 0 &&
      (hslot != priv->selection_slot || cardid + 1 == priv->selection_start_card_id)) {

    /* Try to move the selected cards to the clicked slot */
    if (ar_board_move_selected_cards_to_slot (board, hslot))
      return TRUE;

    /* Move failed if this wasn't the selection_slot slot */
    if (hslot != priv->selection_slot) {
      ar_board_error_bell (board);
    }
  }

  if (hslot != priv->selection_slot ||
      cardid != priv->selection_start_card_id)
    goto set_selection;
    
  /* Single click on the selected slot & card, we take that to mean to deselect,
   * but only in click-to-move mode.
   */
  if (priv->click_to_move) {
    set_selection (board, NULL, -1, FALSE);

    /* Reveal the card on left click */
    reveal_card (board, hslot, cardid);

    return TRUE;
  }

set_selection:

  if (cardid >= 0) {
    drag_valid = aisleriot_game_drag_valid (priv->game,
                                            hslot->id,
                                            hslot->cards->data + cardid,
                                            hslot->cards->len - cardid);
  } else {
    drag_valid = FALSE;
  }

  if (drag_valid) {
    set_selection (board, hslot, cardid, priv->click_to_move);
    priv->click_status = priv->click_to_move ? STATUS_NOT_DRAG : STATUS_MAYBE_DRAG;
  } else {
    set_selection (board, NULL, -1, FALSE);
    priv->click_status = STATUS_NOT_DRAG;
  }

  /* If we're already showing focus or just clicked on the
   * card with the (hidden) focus, show the focus on the
   * clicked card.
   */
  show_focus = priv->show_focus ||
               (hslot == priv->focus_slot &&
                cardid == priv->focus_card_id);
  set_focus (board, hslot, cardid, show_focus);

  /* Reveal the card on left click */
  if (priv->click_to_move) {
    reveal_card (board, hslot, cardid);
  }

  return TRUE;
}

static gboolean
ar_board_button_release (GtkWidget *widget,
                                GdkEventButton *event)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  /* guint state; */

  /* NOTE: It's ok to just return instead of chaining up, since the
   * parent class has no class closure for this event.
   */

  /* We just abort any action on button release, even if the button-up
   * is not the one that started the action. This greatly simplifies the code,
   * and is also the right thing to do, anyway.
   */

  /* state = event->state & gtk_accelerator_get_default_mod_mask (); */

  switch (priv->click_status) {
    case STATUS_SHOW:
      reveal_card (board, NULL, -1);
      break;

    case STATUS_IS_DRAG:
      highlight_drop_target (board, NULL);
      drop_moving_cards (board, event->x, event->y);
      break;

    case STATUS_MAYBE_DRAG:
    case STATUS_NOT_DRAG: {
      ArSlot *slot;
      int card_id;

      /* Don't do the action if the mouse moved away from the clicked slot; see bug #329183 */
      get_slot_and_card_from_point (board, event->x, event->y, &slot, &card_id);
      if (!slot || slot != priv->last_clicked_slot)
        break;

      aisleriot_game_record_move (priv->game, -1, NULL, 0);
      if (aisleriot_game_button_clicked_lambda (priv->game, slot->id)) {
        aisleriot_game_end_move (priv->game);
	ar_sound_play_for_event ("click", (GdkEvent *) event);
      } else {
        aisleriot_game_discard_move (priv->game);
      }

      aisleriot_game_test_end_of_game (priv->game);

      break;
    }

    case STATUS_NONE:
      break;
  }

  priv->click_status = STATUS_NONE;

  set_cursor_by_location (board, event->x, event->y);

  return TRUE;
}

static gboolean
ar_board_motion_notify (GtkWidget *widget,
                               GdkEventMotion *event)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  /* NOTE: It's ok to just return instead of chaining up, since the
   * parent class has no class closure for this event.
   */

  if (priv->show_status_messages) {
    ArSlot *slot = NULL;
    int cardid = -1;

    get_slot_and_card_from_point (board, event->x, event->y, &slot, &cardid);
    if (slot != NULL && ar_slot_get_slot_type (slot) != AR_SLOT_UNKNOWN) {
      char *text;

      text = ar_slot_get_hint_string (slot, cardid);
      set_status_message (board, text);
      g_free (text);
    } else {
      set_status_message (board, NULL);
    }
  }

  if (priv->click_status == STATUS_IS_DRAG) {
    ArSlot *slot, *mslot;
    int x, y;
    cairo_region_t *region;

    x = event->x - priv->last_click_x;
    y = event->y - priv->last_click_y;

    slot = find_drop_target (board, x, y);
    highlight_drop_target (board, slot);

    mslot = priv->moving_cards_slot;
    region = cairo_region_create_rectangle (&mslot->rect);
    mslot->rect.x = x;
    mslot->rect.y = y;
    cairo_region_union_rectangle (region, &mslot->rect);
    gtk_widget_queue_draw_region (widget, region);
    cairo_region_destroy (region);
    if (!slot)
             set_cursor (board, AR_CURSOR_CLOSED);
    else
             set_cursor (board, AR_CURSOR_DROPPABLE);
  } else if (priv->click_status == STATUS_MAYBE_DRAG &&
             gtk_drag_check_threshold (widget,
                                       priv->last_click_x,
                                       priv->last_click_y,
                                       event->x,
                                       event->y)) {
    drag_begin (board);
  } else {
    set_cursor_by_location (board, event->x, event->y);
  }

  return FALSE;
}

static gboolean
ar_board_key_press (GtkWidget *widget,
                           GdkEventKey* event)
{
  return GTK_WIDGET_CLASS (ar_board_parent_class)->key_press_event (widget, event);
}

static gboolean
ar_board_draw (GtkWidget *widget,
                      cairo_t *cr)
{
  ArBoard *board = AR_BOARD (widget);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);
  GtkStyleContext *style_context;
  guint i;
  GPtrArray *slots;
  guint n_slots;
  ArSlot **exposed_slots;
  ArSlot *highlight_slot;
  guint n_exposed_slots;
  GdkRGBA color;
  cairo_surface_t *surface;
  cairo_pattern_t *pattern;
  cairo_matrix_t matrix;
  cairo_rectangle_int_t clip_rect;

  /* NOTE: It's ok to just return instead of chaining up, since the
   * parent class has no class closure for this event.
   */

  if (!gdk_cairo_get_clip_rectangle (cr, &clip_rect))
    return FALSE;

  style_context = gtk_widget_get_style_context (widget);
  /* First paint the background */

  gtk_render_background (style_context,
                         cr, 
                         0, 0,
                         gtk_widget_get_allocated_width (widget),
                         gtk_widget_get_allocated_height (widget));

  /* Only draw the the cards when the geometry is set, and we're in a resize */
  if (!priv->geometry_set) {
    return TRUE;
  }

  /* Now draw the slots and cards */
  slots = aisleriot_game_get_slots (priv->game);

  n_slots = slots->len;

  /* First check which slots are exposed */
  /* It's fine to allocate on the stack, since there'll never be very many slots */
  exposed_slots = g_newa (ArSlot *, n_slots);
  n_exposed_slots = 0;

  for (i = 0; i < n_slots; ++i) {
    ArSlot *slot = slots->pdata[i];

    /* Check whether this slot needs to be drawn */
    exposed_slots[n_exposed_slots++] = slot;
  }

  highlight_slot = priv->highlight_slot;

  /* First draw the slots. Otherwise they'll overlap on cards
   * (e.g. in Elevator, after a card was removed).
   */
  if (priv->slot_surface == NULL)
    goto draw_cards;

  pattern = cairo_pattern_create_for_surface (priv->slot_surface);
  cairo_set_source (cr, pattern);

  for (i = 0; i < n_exposed_slots; ++i) {
    ArSlot *hslot = exposed_slots[i];
    int x, y;

    /* FIXMEchpe: if ((hslot->length - hslot->exposed) >= 0) ?? */
    if (hslot->cards->len > 0)
      continue;

    /* If the slot it empty, draw the slot image */
    x = hslot->rect.x;
    y = hslot->rect.y;

    cairo_matrix_init_translate (&matrix, -x, -y);
    cairo_pattern_set_matrix (pattern, &matrix);
    cairo_paint (cr);

    if (G_UNLIKELY (hslot == highlight_slot)) {
      cairo_save (cr);
      ar_style_get_selection_color (priv->style, &color);
      gdk_cairo_set_source_rgba (cr, &color);
      cairo_mask (cr, pattern);
      cairo_restore (cr);
    }
  }

  cairo_pattern_destroy (pattern);

draw_cards:

  /* Now draw the cards */
  for (i = 0; i < n_exposed_slots; ++i) {
    ArSlot *hslot = exposed_slots[i];
    GByteArray *cards = hslot->cards;
    gpointer *card_images = hslot->card_images->pdata;
    GdkRectangle card_rect;
    guint j, n_cards;
    guint highlight_start_card_id = G_MAXUINT;

    n_cards = cards->len;
    if (n_cards == 0)
      continue;

    if (G_UNLIKELY (hslot == priv->highlight_slot &&
                    priv->show_highlight)) {
      highlight_start_card_id = hslot->cards->len - 1;
    } else if (G_UNLIKELY (hslot == priv->selection_slot &&
                           priv->selection_start_card_id >= 0 &&
                           priv->show_selection)) {
      highlight_start_card_id = priv->selection_start_card_id;
    }

    card_rect.x = hslot->rect.x;
    card_rect.y = hslot->rect.y;
    card_rect.width = priv->card_size.width;
    card_rect.height = priv->card_size.height;

    if (priv->is_rtl &&
        hslot->expanded_right) {
      card_rect.x += hslot->rect.width - priv->card_size.width;
    }

    for (j = n_cards - hslot->exposed; j < n_cards; ++j) {
      /* Check whether this card needs to be drawn */
      /* FIXMEchpe: we can be even smarter here, by checking
       * with the rect of the part of the card that's not going
       * to be obscured by later drawn cards anyway.
       */

      surface = card_images[j];
      if (surface == NULL)
        goto next;

      pattern = cairo_pattern_create_for_surface (surface);
      cairo_matrix_init_translate (&matrix, -card_rect.x, -card_rect.y);
      cairo_pattern_set_matrix (pattern, &matrix);
      cairo_set_source (cr, pattern);
      cairo_pattern_destroy (pattern);

      cairo_paint (cr);

      if (G_UNLIKELY (j >= highlight_start_card_id)) {
        cairo_save (cr);
        ar_style_get_selection_color (priv->style, &color);
        gdk_cairo_set_source_rgba (cr, &color);
        cairo_mask (cr, pattern);
        cairo_restore (cr);
      }

    next:

      card_rect.x += hslot->pixeldx;
      card_rect.y += hslot->pixeldy;
    }
  }

  /* Draw the revealed card */
  if (priv->show_card_slot != NULL)
  {
    GdkRectangle card_rect;

    get_rect_by_slot_and_card (board,
                               priv->show_card_slot,
                               priv->show_card_id,
                               1, &card_rect);

    surface = priv->show_card_slot->card_images->pdata[priv->show_card_id];
    if (surface == NULL)
      goto draw_focus;

    pattern = cairo_pattern_create_for_surface (surface);
    cairo_matrix_init_translate (&matrix, -card_rect.x, -card_rect.y);
    cairo_pattern_set_matrix (pattern, &matrix);
    cairo_set_source (cr, pattern);
    cairo_pattern_destroy (pattern);

    cairo_paint (cr);
  }

draw_focus:

  /* Now draw the moving cards, if any */
  if (priv->moving_cards_slot != NULL) {
    ArSlot *hslot = priv->moving_cards_slot;
    GByteArray *cards = hslot->cards;
    gpointer *card_images = hslot->card_images->pdata;
    GdkRectangle card_rect;
    guint j, n_cards;

    n_cards = cards->len;
    if (n_cards == 0)
      goto expose_done;

    card_rect.x = hslot->rect.x;
    card_rect.y = hslot->rect.y;
    card_rect.width = priv->card_size.width;
    card_rect.height = priv->card_size.height;

    if (priv->is_rtl &&
        hslot->expanded_right) {
      card_rect.x += hslot->rect.width - priv->card_size.width;
    }

    for (j = n_cards - hslot->exposed; j < n_cards; ++j) {
      /* Check whether this card needs to be drawn */

      surface = card_images[j];
      if (surface == NULL)
        goto next_moving_card;

      pattern = cairo_pattern_create_for_surface (surface);
      cairo_matrix_init_translate (&matrix, -card_rect.x, -card_rect.y);
      cairo_pattern_set_matrix (pattern, &matrix);
      cairo_set_source (cr, pattern);
      cairo_pattern_destroy (pattern);

      cairo_paint (cr);

    next_moving_card:

      card_rect.x += hslot->pixeldx;
      card_rect.y += hslot->pixeldy;
    }
  }

expose_done:

  #ifdef DEBUG_DRAWING
  if (cairo_status (cr) != CAIRO_STATUS_SUCCESS) {
    g_print ("expose-event cairo status %d\n", cairo_status (cr));
  }
  #endif

  /* Parent class has no draw handler, no need to chain up */
  return TRUE;
}

/* GObjectClass methods */

static void
ar_board_init (ArBoard *board)
{
  GtkWidget *widget = GTK_WIDGET (board);
  ArBoardPrivate *priv;

  priv = ar_board_get_instance_private (board);

  gtk_widget_set_can_focus (widget, TRUE);

  priv->is_rtl = gtk_widget_get_direction (widget) == GTK_TEXT_DIR_RTL;

  priv->force_geometry_update = FALSE;

  priv->click_to_move = FALSE;
  priv->show_selection = FALSE;
  priv->show_status_messages = FALSE;

  priv->show_card_id = -1;

  priv->card_cache = ar_card_surface_cache_new ();
  // FIXMEchpe connect changed handler

  gtk_widget_set_events (widget,
			 gtk_widget_get_events (widget) |
                         GDK_EXPOSURE_MASK |
                         GDK_BUTTON_PRESS_MASK |
                         GDK_POINTER_MOTION_MASK |
                         GDK_BUTTON_RELEASE_MASK);

  g_signal_connect (widget, "query-tooltip",
                    G_CALLBACK (ar_board_query_tooltip_cb), board);
}

static void
ar_board_dispose (GObject *object)
{
  ArBoard *board = AR_BOARD (object);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  if (priv->style != NULL) {
    _ar_style_gtk_detach (priv->style, GTK_WIDGET (board));

    g_signal_handlers_disconnect_by_func (priv->style,
                                          G_CALLBACK (ar_board_sync_style),
                                          board);

    g_object_unref (priv->style);
    priv->style = NULL;
  }

  G_OBJECT_CLASS (ar_board_parent_class)->dispose (object);
}

static void
ar_board_finalize (GObject *object)
{
  ArBoard *board = AR_BOARD (object);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  g_signal_handlers_disconnect_matched (priv->game,
                                        G_SIGNAL_MATCH_DATA,
                                        0, 0, NULL, NULL, board);
  g_object_unref (priv->game);

  g_object_unref (priv->card_cache);

  G_OBJECT_CLASS (ar_board_parent_class)->finalize (object);
}

static void
ar_board_get_property (GObject *object,
                              guint prop_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  ArBoard *board = AR_BOARD (object);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  switch (prop_id) {
  case PROP_GAME:
    g_value_set_object (value, priv->game);
    break;

  case PROP_STYLE:
    g_value_set_object (value, priv->style);
    break;
  }
}

static void
ar_board_set_property (GObject *object,
                              guint prop_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  ArBoard *board = AR_BOARD (object);
  ArBoardPrivate *priv = ar_board_get_instance_private (board);

  switch (prop_id) {
    case PROP_GAME:
      priv->game = AISLERIOT_GAME (g_value_dup_object (value));

      g_signal_connect (priv->game, "game-type",
                        G_CALLBACK (game_type_changed_cb), board);
      g_signal_connect (priv->game, "game-cleared",
                        G_CALLBACK (game_cleared_cb), board);
      g_signal_connect (priv->game, "game-new",
                        G_CALLBACK (game_new_cb), board);
      g_signal_connect (priv->game, "slot-changed",
                        G_CALLBACK (slot_changed_cb), board);

      break;

    case PROP_STYLE:
      priv->style = g_value_dup_object (value);
      _ar_style_gtk_attach (priv->style, GTK_WIDGET (board));

      ar_board_sync_style (priv->style, NULL, board);
      g_signal_connect (priv->style, "notify",
                        G_CALLBACK (ar_board_sync_style), board);
      break;
  }
}

static void
ar_board_class_init (ArBoardClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gobject_class->dispose = ar_board_dispose;
  gobject_class->finalize = ar_board_finalize;
  gobject_class->get_property = ar_board_get_property;
  gobject_class->set_property = ar_board_set_property;

  widget_class->realize = ar_board_realize;
  widget_class->unrealize = ar_board_unrealize;
  widget_class->size_allocate = ar_board_size_allocate;
  widget_class->get_preferred_width = ar_board_get_preferred_width;
  widget_class->get_preferred_height = ar_board_get_preferred_height;
  widget_class->focus_in_event = ar_board_focus_in;
  widget_class->focus_out_event = ar_board_focus_out;
  widget_class->button_press_event = ar_board_button_press;
  widget_class->button_release_event = ar_board_button_release;
  widget_class->motion_notify_event = ar_board_motion_notify;
  widget_class->key_press_event = ar_board_key_press;
  widget_class->draw = ar_board_draw;

  signals[STATUS_MESSAGE] =
    g_signal_new (I_("status-message"),
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (ArBoardClass, status_message),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);

  /* Properties */
  g_object_class_install_property
    (gobject_class,
     PROP_GAME,
     g_param_spec_object ("game", NULL, NULL,
                          AISLERIOT_TYPE_GAME,
                          G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_property
    (gobject_class,
     PROP_STYLE,
     g_param_spec_object ("style", NULL, NULL,
                          AR_TYPE_STYLE,
                          G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  _ar_style_gtk_class_install_style_properties (widget_class);

  gtk_widget_class_set_css_name(widget_class, "ar-board");
}

/* public API */

GtkWidget *
ar_board_new (ArStyle *style,
              AisleriotGame *game)
{
  return g_object_new (AR_TYPE_BOARD,
                       "style", style,
                       "game", game,
                       NULL);
}

void
ar_board_abort_move (ArBoard *board)
{
  clear_state (board);
}
