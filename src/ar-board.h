/*
 * Copyright © 1998, 2003 Jonathan Blandford <jrb@mit.edu>
 * Copyright © 2007, 2010 Christian Persch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "game.h"
#include "ar-style.h"


G_BEGIN_DECLS


#define AR_TYPE_BOARD    (ar_board_get_type ())
G_DECLARE_DERIVABLE_TYPE (ArBoard, ar_board, AR, BOARD, GtkDrawingArea);

struct _ArBoardClass {
  GtkDrawingAreaClass parent_class;

  void (* status_message)   (ArBoard *board,
                             const char *message);

  /* keybinding signals */
  gboolean (* move_cursor)  (ArBoard *board,
                             GtkMovementStep step,
                             int count);
  void (* activate)         (ArBoard *board);
  void (* toggle_selection) (ArBoard *board);
  void (* select_all)       (ArBoard *board);
  void (* deselect_all)     (ArBoard *board);
};

GType      ar_board_get_type   (void);
GtkWidget *ar_board_new        (ArStyle       *style,
                                AisleriotGame *game);
void       ar_board_abort_move (ArBoard       *board);



G_END_DECLS
